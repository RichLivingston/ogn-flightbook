package main

import (
	"log"
	"sync"
	"time"

	"github.com/go-redis/redis"
	"github.com/gorilla/websocket"
)

const (
	// Time allowed to read the next pong message from the peer.
	pongWait = 30 * time.Second
	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

type Client struct {
	Conn *websocket.Conn
	Pool *Pool
	Red  *redis.Client
	mu   sync.Mutex
}

type Message struct {
	// Message send thought socket
	// Message type=0 -> send last events when client register (max 4 events)
	// Message type=1 -> send the last event (broadcast on all clients)
	Type int         `json:"type"`
	Data interface{} `json:"data"`
}

func (c *Client) ping() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.mu.Unlock()
		c.Conn.Close()
	}()
	for {
		select {
		case <-ticker.C:
			c.mu.Lock()
			if err := c.Conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				log.Println("ping error")
				c.Pool.Unregister <- c
				return
			}
			c.mu.Unlock()
		}
	}
}

func (c *Client) init() {
	var msg Message
	evts := xrev(c.Red)
	msg.Type = 0
	msg.Data = evts
	c.Conn.WriteJSON(msg)
}
