import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'leaflet/dist/leaflet.css';
import VueNativeSock from 'vue-native-websocket'
let location_protocol = document.location.protocol
let location_hostname = document.location.hostname
let ws_protocol = (location_protocol === "https:") ? "wss": 'ws'
let ws_url=`${ws_protocol}://${location_hostname}${process.env.VUE_APP_WS}`
Vue.use(VueNativeSock, ws_url, {format: 'json', reconnection: true, reconnectionAttempts: 3,  store: store})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
